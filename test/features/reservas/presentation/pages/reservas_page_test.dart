import 'package:agendar_canchas/features/reservas/presentation/cubit/reservas_cubit.dart';
import 'package:agendar_canchas/features/reservas/presentation/pages/reservas_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  Widget reservePage() => MultiBlocProvider(
        providers: [
          BlocProvider<ReservasCubit>(
            create: (context) => ReservasCubit(),
          ),
        ],
        child: const MaterialApp(
          home: ReservasPage(),
        ),
      );
  group('ReservasPage', () {
    testWidgets('Renders app bar title correctly', (WidgetTester tester) async {
      await tester.pumpWidget(reservePage());
      await tester.pumpAndSettle();
      expect(find.text('Agendamientos'), findsOneWidget);
      expect(find.byType(FloatingActionButton), findsOneWidget);
    });
  });
}
