import 'package:agendar_canchas/features/reservas/data/datasources/sql_datasource.dart';
import 'package:agendar_canchas/features/reservas/presentation/cubit/reservas_cubit.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'reservas_cubit_test.mocks.dart';

@GenerateMocks([SqlLocalDatasource])
void main() {
  late MockSqlLocalDatasource sqlLocalDatasourceMock;
  late ReservasCubit reservasCubit;

  setUp(() {
    sqlLocalDatasourceMock = MockSqlLocalDatasource();
    reservasCubit = ReservasCubit();
  });

  // tearDown(() {
  //   reservasCubit.close();
  // });

  group(
    'Cases reservas cubit',
    () {
      test('initial state [ReservasInitial]', () async {
        expect(reservasCubit.state.runtimeType, ReservasInitial);
      });

      blocTest(
        'Get weather',
        build: () => reservasCubit,
        act: (cubit) async {
          const expectedText = '10';
          when(
            sqlLocalDatasourceMock.getWeather(any),
          ).thenAnswer((_) async => expectedText);
          await cubit.getWeather();
        },
        expect: () => [isA<WeatherLoaded>()],
      );
    },
  );
}
