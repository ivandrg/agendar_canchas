import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../utils/constants.dart';

class DatabaseHelper {
  static const int _version = 1;
  static const String _dbName = 'reservas_database.db';

  static Future<Database> getDB() async {
    return openDatabase(
      join(await getDatabasesPath(), _dbName),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE ${Constants.nameTable}(id TEXT PRIMARY KEY, cancha TEXT, date TEXT, name TEXT, lluvia TEXT)',
        );
      },
      version: _version,
    );
  }
}
