class Reserva {
  final String id;
  final String cancha;
  final String date;
  final String name;
  final String porcentajeLuvia;

  const Reserva({
    required this.id,
    required this.cancha,
    required this.date,
    required this.name,
    required this.porcentajeLuvia,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'cancha': cancha,
      'date': date,
      'name': name,
      'lluvia': porcentajeLuvia,
    };
  }

  @override
  String toString() {
    return 'Reserva{id: $id, name: $name, cancha: $cancha, date: $date, lluvia: $porcentajeLuvia}';
  }
}
