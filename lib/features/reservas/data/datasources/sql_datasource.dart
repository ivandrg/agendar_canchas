import 'dart:convert';

import 'package:agendar_canchas/utils/constants.dart';
import 'package:sqflite/sql.dart';

import '../../../../services/database_helper.dart';
import '../models/reserva_data_model.dart';
import 'package:http/http.dart' as http;

class SqlLocalDatasource {
  Future<int> insertReserva(Reserva reserva) async {
    final db = await DatabaseHelper.getDB();
    int id = await db.insert(
      Constants.nameTable,
      reserva.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  Future<List<Reserva>> getReservas() async {
    final db = await DatabaseHelper.getDB();
    final List<Map<String, dynamic>> maps = await db.query(Constants.nameTable);
    return List.generate(
      maps.length,
      (i) {
        return Reserva(
          id: maps[i]['id'] as String,
          name: maps[i]['name'] as String,
          cancha: maps[i]['cancha'] as String,
          date: maps[i]['date'] as String,
          porcentajeLuvia: maps[i]['lluvia'] as String,
        );
      },
    );
  }

  Future<void> updateReserva(Reserva reserva) async {
    final db = await DatabaseHelper.getDB();
    await db.update(
      Constants.nameTable,
      reserva.toMap(),
      where: 'id = ?',
      whereArgs: [reserva.id],
    );
  }

  Future<void> deleteReserva(String id) async {
    final db = await DatabaseHelper.getDB();
    await db.delete(
      Constants.nameTable,
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<String> getWeather(String date) async {
    String lat = '1.21361';
    String lon = '-77.28111';
    var headers = {
      'Authorization': 'Basic ZGlzcnVwdGl2ZV9ndXN0aW5faXZhbjpEMVY0OVRidGU1'
    };

    var url =
        'https://api.meteomatics.com/${date}T00:00:00Z/precip_1h:mm/$lat,$lon/json';
    var response = await http.post(
      Uri.parse(url),
      headers: headers,
    );

    if (response.statusCode == 200) {
      final decode = jsonDecode(response.body);
      final lluvia =
          (decode["data"][0]["coordinates"][0]["dates"][0]["value"] * 100)
              .toString();
      return lluvia;
    } else {
      return '';
    }
  }
}
