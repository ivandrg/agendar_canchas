import 'package:agendar_canchas/features/reservas/presentation/cubit/reservas_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class ReservarCanchaWidget extends StatefulWidget {
  const ReservarCanchaWidget({super.key});

  @override
  State<ReservarCanchaWidget> createState() => _ReservarCanchaWidgetState();
}

class _ReservarCanchaWidgetState extends State<ReservarCanchaWidget> {
  @override
  Widget build(BuildContext context) {
    final bloc = context.read<ReservasCubit>();
    return BlocListener<ReservasCubit, ReservasState>(
      listener: (context, state) {
        if (state is AgendaCompleted) {
          Navigator.pop(context);
        } else if (state is AgendaLlena) {
          Navigator.pop(context);
          const snackBar = SnackBar(
            content: Text('Todas las reservas llenas para esa fecha'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      },
      child: Form(
        key: bloc.reservaKey,
        child: ListView(
          padding: const EdgeInsets.all(20),
          children: [
            Text(
              'Reserva tu Cancha',
              style: Theme.of(context).textTheme.titleLarge!.copyWith(
                    fontWeight: FontWeight.w700,
                  ),
            ),
            const SizedBox(height: 20),
            Container(
              width: double.infinity,
              height: 56,
              padding: const EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                  color: Colors.black.withOpacity(0.42),
                ),
              ),
              child: Row(
                children: [
                  Text(
                    bloc.canchaController.text.isEmpty
                        ? 'Selecciona una cancha'
                        : 'Cancha ${bloc.canchaController.text.toUpperCase()}',
                  ),
                  const Spacer(),
                  PopupMenuButton<Canchas>(
                    color: Colors.white,
                    icon: const Icon(
                      Icons.keyboard_arrow_down_rounded,
                      color: Color(0XFF8E8E93),
                    ),
                    itemBuilder: (context) => [
                      PopupMenuItem(
                        value: Canchas.a,
                        child: Text(
                          'Cancha A',
                          style:
                              Theme.of(context).textTheme.bodyMedium!.copyWith(
                                    fontWeight: FontWeight.w700,
                                  ),
                        ),
                      ),
                      PopupMenuItem(
                        value: Canchas.b,
                        child: Text(
                          'Cancha B',
                          style:
                              Theme.of(context).textTheme.bodyMedium!.copyWith(
                                    fontWeight: FontWeight.w700,
                                  ),
                        ),
                      ),
                      PopupMenuItem(
                        value: Canchas.c,
                        child: Text(
                          'Cancha C',
                          style:
                              Theme.of(context).textTheme.bodyMedium!.copyWith(
                                    fontWeight: FontWeight.w700,
                                  ),
                        ),
                      ),
                    ],
                    onSelected: (Canchas item) {
                      setState(() {
                        bloc.canchaController.text = item.name;
                        bloc.filter();
                      });
                    },
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            TextFormField(
              readOnly: true,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Ingrese la fecha de reserva';
                }
                return null;
              },
              controller: bloc.dateController,
              decoration: InputDecoration(
                labelText: 'Fecha de la reserva',
                hintText: 'Fecha de la reserva',
                suffixIcon: GestureDetector(
                  onTap: () async {
                    final outputFormat = DateFormat('dd-MM-yyyy');
                    final outputApiFormat = DateFormat('yyyy-MM-dd');
                    DateTime? picked = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime.now(),
                      lastDate: DateTime(2100),
                    );
                    if (picked != null) {
                      bloc.dateController.text = outputFormat.format(picked);
                      bloc.dateApiController.text =
                          outputApiFormat.format(picked);
                      bloc
                        ..getWeather()
                        ..filter();
                    }
                  },
                  child: const Icon(
                    Icons.calendar_month_outlined,
                    color: Color(0XFF2D6197),
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.black.withOpacity(0.42),
                  ),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0XFF2D6097),
                    width: 2,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            TextFormField(
              textCapitalization: TextCapitalization.words,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Ingrese un Nombre';
                }
                return null;
              },
              controller: bloc.nameController,
              decoration: InputDecoration(
                labelText: 'Nombre',
                hintText: 'Nombre',
                suffixIcon: const Icon(
                  Icons.person_outline,
                  color: Color(0XFF2D6197),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.black.withOpacity(0.42),
                  ),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0XFF2D6097),
                    width: 2,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            BlocBuilder<ReservasCubit, ReservasState>(
              builder: (context, state) {
                return Visibility(
                  visible: bloc.porcentajeLluviaController.text.isNotEmpty,
                  child: Row(
                    children: [
                      Text(
                        'La probabilidad de lluvia para ese día es de ${bloc.porcentajeLluviaController.text} %',
                        style: Theme.of(context).textTheme.bodySmall,
                      ),
                    ],
                  ),
                );
              },
            ),
            const SizedBox(height: 20),
            BlocBuilder<ReservasCubit, ReservasState>(
              builder: (context, state) {
                return Visibility(
                  visible: bloc.reservasFiltradasFecha.length > 2,
                  child: Row(
                    children: [
                      Text(
                        'Todos los cupos de esta cancha estan ocuapdos para este dia',
                        style: Theme.of(context)
                            .textTheme
                            .bodySmall!
                            .copyWith(color: Colors.red),
                      ),
                    ],
                  ),
                );
              },
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                if (bloc.reservaKey.currentState!.validate()) {
                  if (bloc.canchaController.text.isNotEmpty) {
                    bloc.agendarCancha();
                  } else {
                    Navigator.pop(context);
                    const snackBar = SnackBar(
                      content: Text('Seleccione una cancha'),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                }
              },
              child: const Text('Reservar Cancha'),
            ),
          ],
        ),
      ),
    );
  }
}
