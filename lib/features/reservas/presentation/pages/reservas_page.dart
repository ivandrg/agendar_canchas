import 'package:agendar_canchas/features/reservas/presentation/widgets/reservar_cancha_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/reservas_cubit.dart';

class ReservasPage extends StatelessWidget {
  const ReservasPage({super.key});

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<ReservasCubit>();
    // bloc.getReservas();
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Agendamientos',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          showModalBottomSheet<void>(
            isScrollControlled: true,
            context: context,
            builder: (BuildContext context) {
              return ClipRRect(
                borderRadius: BorderRadius.circular(25),
                child: const SizedBox(
                  height: 480,
                  child: ReservarCanchaWidget(),
                ),
              );
            },
          );
        },
        label: const Text('Agendar Cancha'),
      ),
      body: BlocBuilder<ReservasCubit, ReservasState>(
        buildWhen: (previous, current) =>
            current is ReservasLoaded || current is ReservasEmpty,
        builder: (context, state) {
          if (state is ReservasLoaded) {
            return ListView.builder(
              itemCount: state.reservas.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  child: ListTile(
                    title: Row(
                      children: [
                        Text(
                          'Cancha ${state.reservas[index].cancha.toUpperCase()}',
                          style: const TextStyle(
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        const Spacer(),
                        IconButton(
                          onPressed: () {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: const Text('Borrar reserva'),
                                content: const Text('Desea borrar la reserva?'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text('Cancel'),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      bloc.deleteReservas(
                                          state.reservas[index].id);
                                      Navigator.pop(context);
                                    },
                                    child: const Text('OK'),
                                  ),
                                ],
                              ),
                            );
                          },
                          icon: const Icon(
                            Icons.delete_forever,
                            color: Colors.red,
                          ),
                        )
                      ],
                    ),
                    leading: Text(
                      state.reservas[index].date,
                      style: const TextStyle(
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          'Lluvia',
                          style: TextStyle(
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        Text(
                          '${state.reservas[index].porcentajeLuvia} %',
                          style: const TextStyle(
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ],
                    ),
                    subtitle: Text(state.reservas[index].name),
                  ),
                );
              },
            );
          } else if (state is ReservasEmpty) {
            return const Center(
              child: Text('No hay reservas'),
            );
          }
          return const SizedBox();
        },
      ),
    );
  }
}
