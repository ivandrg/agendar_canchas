import 'package:agendar_canchas/features/reservas/data/datasources/sql_datasource.dart';
import 'package:agendar_canchas/features/reservas/data/models/reserva_data_model.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';

part 'reservas_state.dart';

enum Canchas { a, b, c }

class ReservasCubit extends Cubit<ReservasState> {
  ReservasCubit() : super(ReservasInitial());

  final datasource = SqlLocalDatasource();

  TextEditingController canchaController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController porcentajeLluviaController = TextEditingController();
  TextEditingController dateApiController = TextEditingController();
  dynamic reservas;
  List<Reserva> reservasFiltradasCancha = [];
  List<Reserva> reservasFiltradasFecha = [];
  final reservaKey = GlobalKey<FormState>();

  Future<void> agendarCancha() async {
    var uuid = const Uuid();
    filter();
    if (reservasFiltradasFecha.length < 3) {
      datasource.insertReserva(
        Reserva(
          id: uuid.v1(),
          name: nameController.text,
          cancha: canchaController.text,
          date: dateController.text,
          porcentajeLuvia: porcentajeLluviaController.text,
        ),
      );
      emit(AgendaCompleted());
      await getReservas();
      nameController.clear();
      canchaController.clear();
      dateController.clear();
      porcentajeLluviaController.clear();
    } else {
      emit(AgendaLlena());
    }
  }

  void filter() async {
    reservasFiltradasCancha = reservas
        .where((reserva) => reserva.cancha == canchaController.text)
        .toList();
    reservasFiltradasFecha = reservasFiltradasCancha
        .where((reserva) => reserva.date == dateController.text)
        .toList();
  }

  Future<void> getWeather() async {
    porcentajeLluviaController.text =
        await datasource.getWeather(dateApiController.text);
    emit(WeatherLoaded());
  }

  Future<void> getReservas() async {
    reservas = await datasource.getReservas();
    DateFormat outputFormat = DateFormat('dd-MM-yyyy');
    reservas.sort((a, b) {
      var dateA = outputFormat.parse(a.date);
      var dateB = outputFormat.parse(b.date);
      return dateB.compareTo(dateA);
    });
    if (reservas.isEmpty) {
      emit(ReservasEmpty());
    } else {
      emit(ReservasLoaded(reservas: reservas));
    }
  }

  Future<void> deleteReservas(String id) async {
    await datasource.deleteReserva(id);
    await getReservas();
  }
}
