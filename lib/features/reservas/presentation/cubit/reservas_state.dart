part of 'reservas_cubit.dart';

@immutable
sealed class ReservasState {}

final class ReservasInitial extends ReservasState {}

final class WeatherLoaded extends ReservasState {}

final class AgendaLlena extends ReservasState {}

final class AgendaCompleted extends ReservasState {}

final class ReservasEmpty extends ReservasState {}

final class ReservasLoaded extends ReservasState {
  final List<Reserva> reservas;

  ReservasLoaded({required this.reservas});
}
