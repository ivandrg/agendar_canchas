import 'package:agendar_canchas/features/reservas/presentation/cubit/reservas_cubit.dart';
import 'package:agendar_canchas/features/reservas/presentation/pages/reservas_page.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ReservasCubit()..getReservas(),
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: ReservasPage(),
      ),
    );
  }
}
